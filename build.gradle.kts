plugins {
    application
    kotlin("jvm") version "1.3.31"
    kotlin("kapt") version "1.3.31"
}
application { mainClassName = "com.example.main.MainKt" }

kapt { includeCompileClasspath = false }
// borrow from idea behind pull request https://github.com/JetBrains/kotlin/pull/2381
tasks.withType<JavaCompile> { enabled = false }
// slightly more drastic than 2381 fix, but effect is same since project has no .java

// =============================================================================

val tool = project(":tool") {

    apply(plugin = "kotlin")
    apply(plugin = "kotlin-kapt") // for @AutoService
    kapt { includeCompileClasspath = false }
    // 2381 workaround not needed here since no generated .kt to confuse JavaCompile

    val autoService = "com.google.auto.service:auto-service:1.0-rc5"

    repositories { jcenter() }

    dependencies {
        "kapt"(autoService)
        compileOnly(autoService)
        implementation(kotlin("stdlib"))
    }
}

// =============================================================================

repositories { jcenter() }

dependencies {
    "kapt"(tool)
    compileOnly(tool)
    implementation(kotlin("stdlib"))
    implementation(kotlin("reflect"))
}

// =============================================================================
