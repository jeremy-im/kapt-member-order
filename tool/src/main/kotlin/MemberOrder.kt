package com.example.kapt

import com.google.auto.service.AutoService
import java.lang.annotation.Inherited
import javax.annotation.processing.AbstractProcessor
import javax.annotation.processing.RoundEnvironment
import javax.lang.model.SourceVersion
import javax.lang.model.element.Element
import javax.lang.model.element.NestingKind
import javax.lang.model.element.TypeElement
import javax.lang.model.type.TypeMirror
import javax.lang.model.util.ElementFilter
import javax.tools.Diagnostic
import javax.tools.Diagnostic.Kind.*
import javax.tools.StandardLocation

@Inherited
@Retention(AnnotationRetention.SOURCE)
@Target(AnnotationTarget.CLASS)
annotation class MemberOrderIsImportant

@AutoService(javax.annotation.processing.Processor::class)
class MemberOrderProcessor : AbstractProcessor() {

    companion object {
        var debug = true // attaching debugger to processors (running in the compiler) is tricky
    }

    override fun getSupportedAnnotationTypes(): Set<String> =
            setOf(MemberOrderIsImportant::class.java.name)

    override fun getSupportedSourceVersion(): SourceVersion =
            SourceVersion.latestSupported()

    override fun process(annotations: MutableSet<out TypeElement>?, roundEnv: RoundEnvironment): Boolean {

        val elements =
                ElementFilter.typesIn(roundEnv.getElementsAnnotatedWith(MemberOrderIsImportant::class.java))
        if (elements.isEmpty()) {
            if (debug) NOTE("empty round {${annotations?.joinToString()}} (${roundEnv.rootElements.size} root elements)")
            return false
        }

        for (top in elements.filter { it.superclass.toElement() !in elements })
            if (top.nestingKind != NestingKind.TOP_LEVEL)
                ERROR("annotation can only go on top-level", top)
            else {
                val insides = elements.filter { it.goesInside(top, elements) }.toTypedArray()
                val topPackage = top.packageName
                val topObject = "${top.simpleName}_MemberOrder"
                if (debug) NOTE("top $topPackage.$topObject (plus ${insides.size - 1} subclasses)")
                kotlinFile(topPackage, topObject, insides).openWriter().use {
                    it.write("package $topPackage" + "\n" +
                            "import kotlin.reflect.KClass" + "\n" +
                            "object $topObject : (KClass<*>) -> List<String>? {" + "\n" +
                            "override operator fun invoke(kclass: KClass<*>) = map[kclass.qualifiedName]" + "\n" +
                            "private val map: Map<String, List<String>> = mapOf(" + "\n")
                    for ((insideIndex, inside) in insides.withIndex()) {
                        if (insideIndex > 0) it.write(",")
                        it.write(inside.qualifiedName.literalQuote().append(" to listOf(").toString())
                        for ((enclosedIndex, enclosed) in inside.enclosedElements.withIndex()) {
                            if (enclosedIndex > 0) it.write(",")
                            it.write(enclosed.simpleName.literalQuote().toString())
                        }
                        it.write(")" + "\n")
                    }
                    it.write(")" + "\n" + "}")
                }
            }
        return true
    }

    private fun TypeElement.goesInside(top: Element, elements: Set<TypeElement>): Boolean {
        if (this == top) return true
        if (this !in elements) return false
        val up = this.superclass.toElement()
        if (up is TypeElement) return up.goesInside(top, elements)
        return false
    }

    private operator fun Diagnostic.Kind.invoke(message: String): Unit =
            if (this == NOTE) WARNING(message) else // NOTEs swallowed by kapt
                processingEnv.messager.printMessage(this, "MemberOrderProcessor $message")

    private operator fun Diagnostic.Kind.invoke(message: String, element: Element): Unit =
            if (this == NOTE) WARNING(message, element) else // NOTEs swallowed by kapt
                processingEnv.messager.printMessage(this, "MemberOrderProcessor $message", element)

    private fun TypeMirror.toElement() =
            processingEnv.typeUtils.asElement(this)

    private val Element.packageName
        get() = processingEnv.elementUtils.getPackageOf(this).qualifiedName

    private fun kotlinFile(inPackage: CharSequence, objectName: CharSequence, inside: Array<TypeElement>) =
            processingEnv.filer.createResource(StandardLocation.SOURCE_OUTPUT, inPackage, "$objectName.kt", *inside)

    private fun CharSequence.literalQuote(): StringBuilder {
        return (this as? StringBuilder ?: StringBuilder(this)).tripleQuote()
    }

    private fun StringBuilder.tripleQuote(): StringBuilder {
        for (index in (length - 1) downTo 0)
            if (this[index] in "\"\$")
                replace(index, index + 1, "\${'${this[index]}'}")
        return insert(0, "\"\"\"").append("\"\"\"")
    }
}
