package com.example.main

import com.example.kapt.MemberOrderIsImportant

@MemberOrderIsImportant
object Example {

    fun add(by:Int) {
        counter += by
    }

    var counter: Int = 0

    data class Struct(val one: String, val two: String)

    object Listener : (Struct) -> Unit {
        override operator fun invoke(it:Struct) {
            add(it.one.length)
            add(it.two.length)
        }
    }
}

fun main() {
    Example.Listener(Example.Struct("one","two"))

    println(Example_MemberOrder(Example::class)?.joinToString())
    // Struct, Listener, counter, INSTANCE, add, getCounter, setCounter, <init>
}
